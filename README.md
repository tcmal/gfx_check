 # gfx-check

A simple, distro-agnostic script for getting/updating graphics drivers. Also provides machine-friendly interface.

## Commands

All commands support setting a custom repository through `--repo=FILE` or setting a custom distro id through `--distro=ID`.

### interactive

Wizard to easily install graphics drivers:

```
❯❯❯ gfx-check interactive
Choose all applicable graphics cards('1', '1,3', '1-3'):
  1) GT 710
>>> 1
GT 710
Choose a driver:
  1) Proprietary - Official NVIDIA Drivers - http://asdfdasjf
  2) nouveau - Open-Source Community Drivers - http:/asdkjfhjasdkj
>>> 1
Installing driver Proprietary

Packages to install: 
  nvidia
  lib32-nvidia-utils
Generated command: sudo pacman -S nvidia lib32-nvidia-utils
Run this command? THIS COULD HARM YOUR SYSTEM [y/N]: 
```

### list_cards

List detected cards on this machine. Supports `-j` for json output.

### list_drivers

List drivers available for given card. Takes `--slug=card-slug`. Supports `-j` for json output.

### list_packages

List the packages required for a given driver. Takes `--slug=card-slug:driver-slug`. Supports `-j` for json output.

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "gfx_check",
    version = "0.1.0",
    author = "Oscar Shrimpton",
    author_email = "oscar.shrimpton.personal@gmail.com",
    description = ("A simple script for installing graphics card drivers."),
    license = "MIT",
    keywords = "cli script graphics linux distro-agnostic",
    url = "https://gitlab.com/tcmal/gfx-check",
    packages=['gfx_check'],
    long_description=read('README.md'),
    classifiers=[],
	install_requires=['click', 'distro'],
	entry_points={
		'console_scripts': [
			'gfxcheck = gfx_check.main:run'
		]
	}
)

import click
import gfx_check.lib as lib
import os
import sys 
from pathlib import Path
import json
from subprocess import call

@click.group()
@click.option("--repo", help="Data repository to use")
@click.option("--distro", default=lib.get_distro(), help="Distro id to use")
@click.option("-j/-h", default=False, help="Output in JSON / Human readable")
def group(repo, distro, j):
	pass
@group.command("interactive")
@click.option("--repo", help="Data repository to use")
@click.option("--distro", default=lib.get_distro(), help="Distro id to use")
def interactive(repo, distro):
	if not repo:
		repo = lib.get_repo(distro)
	else:
		repo = lib.repo_from_path(Path(repo))
	
	if not repo:
		click.echo(lib.err("Couldn't find repo. Try specifying it manually."))
		sys.exit(1)
	found = lib.find_cards(repo)
	if len(found) < 1:
		click.echo(lib.err("Couldn't find any applicable cards."))
		sys.exit(1)
	chosen = lib.choose_cards(found, repo)
	if len(chosen) > 0:
		to_install = lib.choose_drivers([repo['data'][x['slug']] for x in chosen])
		
		click.echo()
		click.echo(click.style("Packages to install: ", fg="green", bold=True))
		for package in to_install:
			click.echo(
				"  {}".format(
					click.style(package, fg="blue")
				)
			)
		
		command = repo['meta']['install-command'].split(" ")
		command += to_install

		if os.environ['USER'] != "root":
			command = ["sudo"] + command

		click.echo(
			click.style("Generated command: ", bold=True, fg="blue") +
			click.style(" ".join(command), bold=True)
		)
		if click.confirm(
			click.style("Run this command? ", bold=True) + 
			click.style("THIS COULD HARM YOUR SYSTEM", bold=True, fg="red")
		):
			call(command)

@group.command("list_cards")
@click.option("--repo", help="Data repository to use")
@click.option("--distro", default=lib.get_distro(), help="Distro id to use")
@click.option("-j/-h", default=False, help="Output in JSON / Human readable")
def list_cards(repo, distro, j):
	if not repo:
		repo = lib.get_repo(distro)
	else:
		repo = lib.repo_from_path(Path(repo))
	
	if not repo:
		print("Couldn't find repo. Try specifying it manually.")
		sys.exit(1)
	
	found = lib.find_cards(repo)
	output = []
	for card in found:
		data = repo['data'][card['slug']]
		if j:
			output.append(data)
		else:
			output.append(
				"{} - {} drivers available - {}".format(
					click.style(data['friendly'], bold=True, fg="green"),
					click.style(str(len(data['drivers'])), bold=True, fg="yellow"),
					click.style(card['slug'])
				)
			)
	if j:
		click.echo(json.dumps(output))
	else:
		click.echo("\n".join(output))

@group.command("list_drivers")
@click.option("--slug", help="Slug of card to get drivers for", multiple = True)
@click.option("--repo", help="Data repository to use")
@click.option("--distro", default=lib.get_distro(), help="Distro id to use")
@click.option("-j/-h", default=False, help="Output in JSON / Human readable")
def list_drivers(slug, repo, distro, j):
	if not repo:
		repo = lib.get_repo(distro)
	else:
		repo = lib.repo_from_path(Path(repo))
	
	if not repo:
		print("Couldn't find repo. Try specifying it manually.")
		sys.exit(1)

	output = []
	for card_slug in [x for x in slug if x in repo['data']]:
		card = repo['data'][card_slug]
		if j:
			output.append(card)
		else:
			output.append(click.style(card_slug, bold=True, fg="green"))
			output += [
				"  {} - {} - {} - {} packages - {}".format(
					click.style(driver['title'], bold=True, fg="yellow"),
					click.style(driver['description']),
					click.style(driver['license']),
					click.style(str(len(driver['packages'])), fg="blue"),
					click.style(driver['slug'])
				) for driver in card['drivers']
			]
	if j:
		click.echo(json.dumps(output))
	else:
		click.echo("\n".join(output))

@group.command("list_packages")
@click.option("--slug", help="Slug of card to get drivers for", multiple = True)
@click.option("--repo", help="Data repository to use")
@click.option("--distro", default=lib.get_distro(), help="Distro id to use")
@click.option("-j/-h", default=False, help="Output in JSON / Human readable")
def list_packages(slug, repo, distro, j):
	if not repo:
		repo = lib.get_repo(distro)
	else:
		repo = lib.repo_from_path(Path(repo))
	
	if not repo:
		print("Couldn't find repo. Try specifying it manually.")
		sys.exit(1)

	output = []
	for given_slug in slug:
		card_slug = given_slug.split(":")[0]
		driver_slug = given_slug.split(":")[1]
		card = None
		if card_slug in repo['data']:
			card = repo['data'][card_slug]
		if j:
			output.append(card)
		else:
			if card:
				output.append(click.style(given_slug, bold=True, fg="yellow"))
				driver = [x for x in card['drivers'] if x['slug'] == driver_slug]
				if len(driver) > 0:
					for package in driver[0]['packages']:
						output += [
							"  {}".format(
								click.style(package, bold=True, fg="blue")
							)
						]
				else:
					output.append(err("  Couldn't find driver " + driver_slug))
			else:
				output.append(err("Couldn't find card " + card_slug))
	if j:
		click.echo(json.dumps(output))
	else:
		click.echo("\n".join(output))

def run():
	group()

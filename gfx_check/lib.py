import click
import distro

from pathlib import Path
import os
import json
import sys
from subprocess import run
import re

def err(string):
	return click.style(string, fg="red", bold=True)

def choose_from_mult(prompt, options, display):
	print(prompt)
	for idx, element in enumerate(options):
		print("  {} {}".format(click.style(str(idx + 1) + ")", bold=True, fg="yellow"), display(element)))
	i = input(click.style(">>> ", bold=True))
	try:
		chosen = []
		if "," in i:
			for x in i.split(","):
				chosen.append(int(x))
		elif "-" in i:
			between = list(map(lambda x: int(x), i.split("-")))
			biggest = max(between)
			smallest = min(between)
			chosen += range(smallest, biggest+1)
		else: 
			chosen.append(int(i))
		if len([x for x in chosen if x < 0 or x > len(options)]) > 0:
			return None
		return [x - 1 for x in chosen]
	except:
		pass
	return None

def choose_from_single(prompt, options, display):
	print(prompt)
	for idx, element in enumerate(options):
		print("  {} {}".format(click.style(str(idx + 1) + ")", bold=True, fg="yellow"), display(element)))
	i = input(click.style(">>> ", bold=True))
	try:
		if int(i) > 0 and int(i) <= len(options):
			return int(i) -1
	except:
		pass
	return None

def get_distro():
	return distro.id()

def repo_from_path(path):
	if not path.exists():
		return None

	repo_file = open(path)
	return json.load(repo_file)

def get_repo(distro):
	repo_path = Path(os.path.dirname(os.path.abspath(__file__))) / 'data' / (distro + ".json")

	if not repo_path.exists():
		return None

	repo_file = open(repo_path)
	return json.load(repo_file)

def find_cards(repo):
	lspci_raw = run(["lspci"], capture_output=True)
	if lspci_raw.returncode != 0:
		return err("Error running lspci (returned {}): {}".format(lspci_raw.returncode, lspci_raw.stderr))

	lspci = lspci_raw.stdout
	found = []

	for filter,possibilities in repo['filters'].items():
		filtered = [str(x) for x in lspci.splitlines() if filter in str(x)]
		for possibility in possibilities:
			p = re.compile(possibility['regex'])
			matches = [x for x in filtered if p.search(x) != None]
			if len(matches) > 0:
				possibility['matches'] = matches
				found.append(possibility)
	return found

def choose_cards(found, repo):
	chosen = choose_from_mult(
		click.style("Choose all applicable graphics cards('1', '1,3', '1-3'):", fg="magenta", bold=True), 
		found, 
		lambda x: click.style(repo['data'][x['slug']]['friendly'], fg="green", bold=True)
	)
	if chosen == None:
		print("Unrecognised choice")
		return []
	return [found[x] for x in chosen]

def choose_drivers(cards):
	to_install = []
	for card in cards: 
		print(click.style(card['friendly'], fg="green", bold=True))
		chosen = choose_from_single(
			click.style("Choose a driver:", fg="magenta", bold=True),
			card['drivers'],
			lambda x: click.style(x['title'], fg="blue") + " - " + x['description'] + ' - ' + x['license']
		)
		if chosen != None:
			to_install += card['drivers'][chosen]['packages']
			print(click.style(
				"Installing driver {}".format(card['drivers'][chosen]['title']),
				fg="blue"
			))
		else:
			print(err("Invalid driver entered. Skipping this card."))
	return to_install
